# Sobre o Projeto

O projeto tem como objetivo monitorar o fluxo de entrada e saida de distribuidoras prevendo baixas, pontos de reposição e itens ideais a serem repostos nos estoques de distribuidoras ligadas a industria.

Para saber mais sobre o projeto, acesse a nossa [wiki](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home).


# Andamento do Projeto

- [x] Definições do Projeto
- [ ] Documentação da Solução
- [ ] Desenvolvimento da Solução (Sprint 1)
- [ ] Desenvolvimento da Solução (Sprint 2)
- [ ] Desenvolvimento da Solução (Sprint 3)
- [ ] Desenvolvimento da Solução (Sprint 4)
- [ ] Fechamento do Projeto

    
# Equipe

* [Matheus Santi (SM)](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Guilherme Oliveira (PO)](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Felipe Stefani](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Jéssica Vicentini](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Vinicius Mapelli](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [João Vitor Biston](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Davi Dutra](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Gabriel Santos](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)
* [Maransatto](https://gitlab.com/BDAg/pede-mais-marilam/wikis/home)