import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './layout.component';
// import { LayoutRoutingModule } from './layout-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LayoutComponent,
    FooterComponent,
    SideMenuComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    // LayoutRoutingModule
  ],
  exports: [
    LayoutComponent,
    FooterComponent,
    SideMenuComponent,
    HeaderComponent
  ],
})
export class LayoutModule { }
