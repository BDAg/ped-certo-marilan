import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutRoutingComponent } from './layout-routing.component';
import { LayoutComponent } from './layout.component';
import { EmpresasModule } from '../empresas/empresas.module';

const routes: Routes = [
  {
    path: '',
    component: LayoutRoutingComponent,
    children: [
      {
        path: '',
        component: LayoutComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    EmpresasModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [LayoutRoutingComponent]
})
export class LayoutRoutingModule { }
