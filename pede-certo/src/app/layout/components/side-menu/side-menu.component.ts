import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MeGQL } from 'src/app/services/pedeCertoGraphql.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  me: any;

  constructor(
    private router: Router,
    private meService: MeGQL
  ) { }

  ngOnInit() {
    this.getMe();
  }

  getMe() {
    this.meService.fetch({}).pipe(
      map(res => res.data.me)
    ).subscribe(me => {
      this.me = me;
    });
  }

  get url() {
    return this.router.url;
  }

}
