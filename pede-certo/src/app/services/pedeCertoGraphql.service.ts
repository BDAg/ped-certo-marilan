import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** The `Upload` scalar type represents a file upload. */
  Upload: any,
};




export enum AllUserRoleType {
  Seller = 'SELLER',
  Admin = 'ADMIN'
}

export enum CacheControlScope {
  Public = 'PUBLIC',
  Private = 'PRIVATE'
}

export type Categoria = {
   __typename?: 'Categoria',
  id: Scalars['ID'],
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
};

export type CategoriaEdge = {
   __typename?: 'CategoriaEdge',
  node: Categoria,
};

export type CategoriaList = {
   __typename?: 'CategoriaList',
  totalCount: Scalars['Int'],
  pageInfo: PageInfo,
  edges: Array<CategoriaEdge>,
};

export type CreateAdminInput = {
  email: Scalars['String'],
  password: Scalars['String'],
  name: Scalars['String'],
  role?: Maybe<Scalars['String']>,
};

export type CreateCategoriaInput = {
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
};

export type CreatePedido_ProdutoInput = {
  pedidoId: Scalars['ID'],
  preco: Scalars['Float'],
  produtoId: Scalars['ID'],
  quantidade: Scalars['Int'],
};

export type CreatePedidoInput = {
  dealerId: Scalars['ID'],
  preco_total: Scalars['Float'],
  data_pedido: Scalars['String'],
};

export type CreateProdutoInput = {
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
  preco: Scalars['Float'],
  altura?: Maybe<Scalars['Float']>,
  largura?: Maybe<Scalars['Float']>,
  profundidade?: Maybe<Scalars['Float']>,
  peso?: Maybe<Scalars['Float']>,
  image_src?: Maybe<Scalars['String']>,
  categoriaId: Scalars['ID'],
};

export type CreateUserInput = {
  email: Scalars['String'],
  password: Scalars['String'],
  name: Scalars['String'],
  cnpj: Scalars['String'],
  role: UserRoleType,
  phone?: Maybe<Scalars['String']>,
};

export type Mutation = {
   __typename?: 'Mutation',
  createCategoria: Categoria,
  updateCategoria: Categoria,
  deleteCategoria: Categoria,
  createPedido: Pedido,
  updatePedido: Pedido,
  deletePedido: Pedido,
  createPedido_Produto: Pedido_Produto,
  updatePedido_Produto: Pedido_Produto,
  deletePedido_Produto: Pedido_Produto,
  createProduto: Produto,
  updateProduto: Produto,
  deleteProduto: Produto,
  createUser: User,
  createAdmin: User,
  updateUser: User,
  updateUserProfile: User,
  deleteUser: User,
  login?: Maybe<Scalars['String']>,
  logout: Scalars['Boolean'],
};


export type MutationCreateCategoriaArgs = {
  input: CreateCategoriaInput
};


export type MutationUpdateCategoriaArgs = {
  id: Scalars['ID'],
  input: UpdateCategoriaInput
};


export type MutationDeleteCategoriaArgs = {
  id: Scalars['ID']
};


export type MutationCreatePedidoArgs = {
  input: CreatePedidoInput
};


export type MutationUpdatePedidoArgs = {
  id: Scalars['ID'],
  input: UpdatePedidoInput
};


export type MutationDeletePedidoArgs = {
  id: Scalars['ID']
};


export type MutationCreatePedido_ProdutoArgs = {
  input: CreatePedido_ProdutoInput
};


export type MutationUpdatePedido_ProdutoArgs = {
  id: Scalars['ID'],
  input: UpdatePedido_ProdutoInput
};


export type MutationDeletePedido_ProdutoArgs = {
  id: Scalars['ID']
};


export type MutationCreateProdutoArgs = {
  input: CreateProdutoInput
};


export type MutationUpdateProdutoArgs = {
  id: Scalars['ID'],
  input: UpdateProdutoInput
};


export type MutationDeleteProdutoArgs = {
  id: Scalars['ID']
};


export type MutationCreateUserArgs = {
  input: CreateUserInput
};


export type MutationCreateAdminArgs = {
  input: CreateAdminInput
};


export type MutationUpdateUserArgs = {
  id: Scalars['ID'],
  input: UpdateUserInput
};


export type MutationUpdateUserProfileArgs = {
  id: Scalars['ID'],
  input: UpdateUserProfileInput
};


export type MutationDeleteUserArgs = {
  id: Scalars['ID']
};


export type MutationLoginArgs = {
  email: Scalars['String'],
  password: Scalars['String']
};

export type PageInfo = {
   __typename?: 'PageInfo',
  page: Scalars['Int'],
  pageSize: Scalars['Int'],
  pageCount: Scalars['Int'],
  hasNextPage: Scalars['Boolean'],
  hasPrevPage: Scalars['Boolean'],
};

export type Pedido = {
   __typename?: 'Pedido',
  id: Scalars['ID'],
  dealer: User,
  preco_total?: Maybe<Scalars['Float']>,
  data_pedido: Scalars['String'],
};

export type Pedido_Produto = {
   __typename?: 'Pedido_Produto',
  id: Scalars['ID'],
  pedido: Pedido,
  preco?: Maybe<Scalars['Float']>,
  produto: Produto,
  quantidade?: Maybe<Scalars['Int']>,
};

export type Pedido_ProdutoEdge = {
   __typename?: 'Pedido_ProdutoEdge',
  node: Pedido_Produto,
};

export type Pedido_ProdutoList = {
   __typename?: 'Pedido_ProdutoList',
  totalCount: Scalars['Int'],
  pageInfo: PageInfo,
  edges: Array<Pedido_ProdutoEdge>,
};

export type PedidoEdge = {
   __typename?: 'PedidoEdge',
  node: Pedido,
};

export type PedidoList = {
   __typename?: 'PedidoList',
  totalCount: Scalars['Int'],
  pageInfo: PageInfo,
  edges: Array<PedidoEdge>,
};

export type Produto = {
   __typename?: 'Produto',
  id: Scalars['ID'],
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
  preco: Scalars['Float'],
  altura?: Maybe<Scalars['Float']>,
  largura?: Maybe<Scalars['Float']>,
  profundidade?: Maybe<Scalars['Float']>,
  peso?: Maybe<Scalars['Float']>,
  image_src?: Maybe<Scalars['String']>,
  categoria?: Maybe<Categoria>,
};

export type ProdutoEdge = {
   __typename?: 'ProdutoEdge',
  node: Produto,
};

export type ProdutoList = {
   __typename?: 'ProdutoList',
  totalCount: Scalars['Int'],
  pageInfo: PageInfo,
  edges: Array<ProdutoEdge>,
};

export type Query = {
   __typename?: 'Query',
  categoria: Categoria,
  categorias: CategoriaList,
  filtroCategorias: CategoriaList,
  pedido: Pedido,
  pedidos: PedidoList,
  findPedidoByUser: PedidoList,
  pedido_produto: Pedido_Produto,
  pedido_produtos: Pedido_ProdutoList,
  findProdutoByPedido: Pedido_ProdutoList,
  produto: Produto,
  produtos: ProdutoList,
  findProdutoByCategoria: ProdutoList,
  me: User,
  user: User,
  users: UserList,
};


export type QueryCategoriaArgs = {
  id: Scalars['ID']
};


export type QueryCategoriasArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>
};


export type QueryFiltroCategoriasArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  nome?: Maybe<Scalars['String']>,
  descricao?: Maybe<Scalars['String']>
};


export type QueryPedidoArgs = {
  id: Scalars['ID']
};


export type QueryPedidosArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>
};


export type QueryFindPedidoByUserArgs = {
  userId: Scalars['ID']
};


export type QueryPedido_ProdutoArgs = {
  id: Scalars['ID']
};


export type QueryPedido_ProdutosArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>
};


export type QueryFindProdutoByPedidoArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  pedidoId: Scalars['ID']
};


export type QueryProdutoArgs = {
  id: Scalars['ID']
};


export type QueryProdutosArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>
};


export type QueryFindProdutoByCategoriaArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  categoriaId: Scalars['ID']
};


export type QueryUserArgs = {
  id: Scalars['ID']
};


export type QueryUsersArgs = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  role?: Maybe<Array<Maybe<AllUserRoleType>>>,
  search?: Maybe<Scalars['String']>
};

export type UpdateAdminInput = {
  email?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  role?: Maybe<Scalars['String']>,
};

export type UpdateCategoriaInput = {
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
};

export type UpdatePedido_ProdutoInput = {
  pedidoId: Scalars['ID'],
  preco: Scalars['Float'],
  produtoId: Scalars['ID'],
  quantidade: Scalars['Int'],
};

export type UpdatePedidoInput = {
  preco_total?: Maybe<Scalars['Float']>,
  data_pedido?: Maybe<Scalars['String']>,
};

export type UpdateProdutoInput = {
  nome?: Maybe<Scalars['String']>,
  descricao?: Maybe<Scalars['String']>,
  preco?: Maybe<Scalars['Float']>,
  image_src?: Maybe<Scalars['String']>,
  altura?: Maybe<Scalars['Float']>,
  largura?: Maybe<Scalars['Float']>,
  profundidade?: Maybe<Scalars['Float']>,
  peso?: Maybe<Scalars['Float']>,
  categoriaId?: Maybe<Scalars['ID']>,
};

export type UpdateUserInput = {
  email?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  role?: Maybe<UserRoleType>,
  phone?: Maybe<Scalars['String']>,
  cnpj?: Maybe<Scalars['String']>,
};

export type UpdateUserProfileInput = {
  name?: Maybe<Scalars['String']>,
  cnpj?: Maybe<Scalars['String']>,
  phone?: Maybe<Scalars['String']>,
};


export type User = {
   __typename?: 'User',
  id: Scalars['ID'],
  email: Scalars['String'],
  name: Scalars['String'],
  cnpj: Scalars['String'],
  role: AllUserRoleType,
  phone?: Maybe<Scalars['String']>,
};

export type UserEdge = {
   __typename?: 'UserEdge',
  node: User,
};

export type UserList = {
   __typename?: 'UserList',
  totalCount: Scalars['Int'],
  pageInfo: PageInfo,
  edges: Array<UserEdge>,
};

export enum UserRoleType {
  Dealer = 'DEALER',
  Admin = 'ADMIN'
}

export type CriarPedidoMutationVariables = {
  dealerId: Scalars['ID'],
  preco_total: Scalars['Float'],
  data_pedido: Scalars['String']
};


export type CriarPedidoMutation = (
  { __typename?: 'Mutation' }
  & { createPedido: (
    { __typename?: 'Pedido' }
    & Pick<Pedido, 'id'>
  ) }
);

export type InserirProdutoPedidoMutationVariables = {
  pedidoId: Scalars['ID'],
  preco: Scalars['Float'],
  produtoId: Scalars['ID'],
  quantidade: Scalars['Int']
};


export type InserirProdutoPedidoMutation = (
  { __typename?: 'Mutation' }
  & { createPedido_Produto: (
    { __typename?: 'Pedido_Produto' }
    & Pick<Pedido_Produto, 'id'>
  ) }
);

export type PedidosRealizadosQueryVariables = {
  userId: Scalars['ID']
};


export type PedidosRealizadosQuery = (
  { __typename?: 'Query' }
  & { findPedidoByUser: (
    { __typename?: 'PedidoList' }
    & { edges: Array<(
      { __typename?: 'PedidoEdge' }
      & { node: (
        { __typename?: 'Pedido' }
        & Pick<Pedido, 'id' | 'preco_total' | 'data_pedido'>
        & { dealer: (
          { __typename?: 'User' }
          & Pick<User, 'id' | 'name'>
        ) }
      ) }
    )> }
  ) }
);

export type ProdutosPedidosQueryVariables = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>,
  pedidoId: Scalars['ID']
};


export type ProdutosPedidosQuery = (
  { __typename?: 'Query' }
  & { findProdutoByPedido: (
    { __typename?: 'Pedido_ProdutoList' }
    & Pick<Pedido_ProdutoList, 'totalCount'>
    & { pageInfo: (
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'pageCount' | 'hasNextPage' | 'hasPrevPage'>
    ), edges: Array<(
      { __typename?: 'Pedido_ProdutoEdge' }
      & { node: (
        { __typename?: 'Pedido_Produto' }
        & Pick<Pedido_Produto, 'id' | 'preco' | 'quantidade'>
        & { pedido: (
          { __typename?: 'Pedido' }
          & Pick<Pedido, 'id' | 'preco_total' | 'data_pedido'>
        ), produto: (
          { __typename?: 'Produto' }
          & Pick<Produto, 'nome' | 'preco' | 'peso' | 'altura' | 'profundidade' | 'largura'>
        ) }
      ) }
    )> }
  ) }
);

export type CadastrarProdutoMutationVariables = {
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
  preco: Scalars['Float'],
  image_src: Scalars['String'],
  categoriaId: Scalars['ID'],
  altura?: Maybe<Scalars['Float']>,
  largura?: Maybe<Scalars['Float']>,
  profundidade?: Maybe<Scalars['Float']>,
  peso?: Maybe<Scalars['Float']>
};


export type CadastrarProdutoMutation = (
  { __typename?: 'Mutation' }
  & { createProduto: (
    { __typename?: 'Produto' }
    & Pick<Produto, 'id'>
  ) }
);

export type CategoriasProdutosQueryVariables = {
  page?: Maybe<Scalars['Int']>,
  pageSize?: Maybe<Scalars['Int']>
};


export type CategoriasProdutosQuery = (
  { __typename?: 'Query' }
  & { categorias: (
    { __typename?: 'CategoriaList' }
    & { edges: Array<(
      { __typename?: 'CategoriaEdge' }
      & { node: (
        { __typename?: 'Categoria' }
        & Pick<Categoria, 'id' | 'nome' | 'descricao'>
      ) }
    )> }
  ) }
);

export type DadosProdutoQueryVariables = {
  id: Scalars['ID']
};


export type DadosProdutoQuery = (
  { __typename?: 'Query' }
  & { produto: (
    { __typename?: 'Produto' }
    & Pick<Produto, 'id' | 'nome' | 'descricao' | 'preco' | 'image_src' | 'altura' | 'largura' | 'profundidade' | 'peso'>
    & { categoria: Maybe<(
      { __typename?: 'Categoria' }
      & Pick<Categoria, 'id'>
    )> }
  ) }
);

export type EditarProdutoMutationVariables = {
  id: Scalars['ID'],
  nome: Scalars['String'],
  descricao?: Maybe<Scalars['String']>,
  preco: Scalars['Float'],
  image_src: Scalars['String'],
  categoriaId: Scalars['ID']
};


export type EditarProdutoMutation = (
  { __typename?: 'Mutation' }
  & { updateProduto: (
    { __typename?: 'Produto' }
    & Pick<Produto, 'id'>
  ) }
);

export type GetProdutosQueryVariables = {
  pageSize: Scalars['Int'],
  page: Scalars['Int']
};


export type GetProdutosQuery = (
  { __typename?: 'Query' }
  & { produtos: (
    { __typename?: 'ProdutoList' }
    & Pick<ProdutoList, 'totalCount'>
    & { pageInfo: (
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'hasNextPage' | 'hasPrevPage' | 'pageCount'>
    ), edges: Array<(
      { __typename?: 'ProdutoEdge' }
      & { node: (
        { __typename?: 'Produto' }
        & Pick<Produto, 'id' | 'nome' | 'preco' | 'descricao' | 'image_src' | 'altura' | 'largura' | 'profundidade' | 'peso'>
        & { categoria: Maybe<(
          { __typename?: 'Categoria' }
          & Pick<Categoria, 'id' | 'nome'>
        )> }
      ) }
    )> }
  ) }
);

export type DeletarProdutoMutationVariables = {
  ID: Scalars['ID']
};


export type DeletarProdutoMutation = (
  { __typename?: 'Mutation' }
  & { deleteProduto: (
    { __typename?: 'Produto' }
    & Pick<Produto, 'id'>
  ) }
);

export type MeQueryVariables = {};


export type MeQuery = (
  { __typename?: 'Query' }
  & { me: (
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name' | 'email' | 'role'>
  ) }
);

export type LoginMutationVariables = {
  email: Scalars['String'],
  password: Scalars['String']
};


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'login'>
);


export const CriarPedidoDocument = gql`
    mutation criarPedido($dealerId: ID!, $preco_total: Float!, $data_pedido: String!) {
  createPedido(input: {dealerId: $dealerId, preco_total: $preco_total, data_pedido: $data_pedido}) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CriarPedidoGQL extends Apollo.Mutation<CriarPedidoMutation, CriarPedidoMutationVariables> {
    document = CriarPedidoDocument;
    
  }
export const InserirProdutoPedidoDocument = gql`
    mutation inserirProdutoPedido($pedidoId: ID!, $preco: Float!, $produtoId: ID!, $quantidade: Int!) {
  createPedido_Produto(input: {pedidoId: $pedidoId, preco: $preco, produtoId: $produtoId, quantidade: $quantidade}) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class InserirProdutoPedidoGQL extends Apollo.Mutation<InserirProdutoPedidoMutation, InserirProdutoPedidoMutationVariables> {
    document = InserirProdutoPedidoDocument;
    
  }
export const PedidosRealizadosDocument = gql`
    query pedidosRealizados($userId: ID!) {
  findPedidoByUser(userId: $userId) {
    edges {
      node {
        id
        preco_total
        data_pedido
        dealer {
          id
          name
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class PedidosRealizadosGQL extends Apollo.Query<PedidosRealizadosQuery, PedidosRealizadosQueryVariables> {
    document = PedidosRealizadosDocument;
    
  }
export const ProdutosPedidosDocument = gql`
    query produtosPedidos($page: Int = 1, $pageSize: Int = 10, $pedidoId: ID!) {
  findProdutoByPedido(page: $page, pageSize: $pageSize, pedidoId: $pedidoId) {
    totalCount
    pageInfo {
      pageCount
      hasNextPage
      hasPrevPage
    }
    edges {
      node {
        id
        preco
        quantidade
        pedido {
          id
          preco_total
          data_pedido
        }
        produto {
          nome
          preco
          peso
          altura
          profundidade
          largura
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ProdutosPedidosGQL extends Apollo.Query<ProdutosPedidosQuery, ProdutosPedidosQueryVariables> {
    document = ProdutosPedidosDocument;
    
  }
export const CadastrarProdutoDocument = gql`
    mutation cadastrarProduto($nome: String!, $descricao: String, $preco: Float!, $image_src: String!, $categoriaId: ID!, $altura: Float, $largura: Float, $profundidade: Float, $peso: Float) {
  createProduto(input: {nome: $nome, descricao: $descricao, preco: $preco, largura: $largura, altura: $altura, profundidade: $profundidade, peso: $peso, image_src: $image_src, categoriaId: $categoriaId}) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CadastrarProdutoGQL extends Apollo.Mutation<CadastrarProdutoMutation, CadastrarProdutoMutationVariables> {
    document = CadastrarProdutoDocument;
    
  }
export const CategoriasProdutosDocument = gql`
    query categoriasProdutos($page: Int = 1, $pageSize: Int = 10) {
  categorias(page: $page, pageSize: $pageSize) {
    edges {
      node {
        id
        nome
        descricao
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CategoriasProdutosGQL extends Apollo.Query<CategoriasProdutosQuery, CategoriasProdutosQueryVariables> {
    document = CategoriasProdutosDocument;
    
  }
export const DadosProdutoDocument = gql`
    query dadosProduto($id: ID!) {
  produto(id: $id) {
    id
    nome
    descricao
    preco
    image_src
    altura
    largura
    profundidade
    peso
    categoria {
      id
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DadosProdutoGQL extends Apollo.Query<DadosProdutoQuery, DadosProdutoQueryVariables> {
    document = DadosProdutoDocument;
    
  }
export const EditarProdutoDocument = gql`
    mutation editarProduto($id: ID!, $nome: String!, $descricao: String, $preco: Float!, $image_src: String!, $categoriaId: ID!) {
  updateProduto(id: $id, input: {nome: $nome, descricao: $descricao, preco: $preco, image_src: $image_src, categoriaId: $categoriaId}) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EditarProdutoGQL extends Apollo.Mutation<EditarProdutoMutation, EditarProdutoMutationVariables> {
    document = EditarProdutoDocument;
    
  }
export const GetProdutosDocument = gql`
    query getProdutos($pageSize: Int!, $page: Int!) {
  produtos(pageSize: $pageSize, page: $page) {
    totalCount
    pageInfo {
      hasNextPage
      hasPrevPage
      pageCount
    }
    edges {
      node {
        id
        nome
        preco
        descricao
        image_src
        altura
        largura
        profundidade
        peso
        categoria {
          id
          nome
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetProdutosGQL extends Apollo.Query<GetProdutosQuery, GetProdutosQueryVariables> {
    document = GetProdutosDocument;
    
  }
export const DeletarProdutoDocument = gql`
    mutation deletarProduto($ID: ID!) {
  deleteProduto(id: $ID) {
    id
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DeletarProdutoGQL extends Apollo.Mutation<DeletarProdutoMutation, DeletarProdutoMutationVariables> {
    document = DeletarProdutoDocument;
    
  }
export const MeDocument = gql`
    query me {
  me {
    id
    name
    email
    role
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MeGQL extends Apollo.Query<MeQuery, MeQueryVariables> {
    document = MeDocument;
    
  }
export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginGQL extends Apollo.Mutation<LoginMutation, LoginMutationVariables> {
    document = LoginDocument;
    
  }