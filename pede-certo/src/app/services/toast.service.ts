import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  opt = {
    progressBar: true,
    timeOut: 3000
  };
  constructor(
    private toastr: ToastrService
  ) { }

  sucesso(mensagem: string, titulo = '', options = {}) {
    this.toastr.success(mensagem, titulo, Object.assign(this.opt, options));
  }

  erro(mensagem: string, titulo = '', options = {}) {
    this.toastr.error(mensagem, titulo, Object.assign(this.opt, options));
  }
}
