import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoginGQL, MeGQL } from '../services/pedeCertoGraphql.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  erro = false;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginGQL,
    private meService: MeGQL,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: null,
      password: null
    });
  }

  logIn() {
    this.erro = false;
    this.loginService.mutate(this.form.value)
      .subscribe(data => {
        const token = data.data.login;
        if (token) {
          this.erro = false;
          localStorage.setItem('token', token);
          this.meService.fetch({}).pipe(
            map(res => res.data.me)
          ).subscribe(me => {
            if (me.role === 'SELLER') {
              this.router.navigate(['/empresa/produtos']);
            } else {
              this.router.navigate(['/produtos']);
            }
          }, error => {
            console.log(error);
          });
        } else {
          this.erro = true;
        }
      }, error => {
        console.log(error);
      }
    );
  }
}
