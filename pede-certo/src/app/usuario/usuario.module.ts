import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { GraphQLModule } from '../graphql.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { UsuarioRoutingModule } from './usuario-routing.module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    GraphQLModule,
    ReactiveFormsModule,
    UsuarioRoutingModule
  ],
  exports: [
    RouterModule
  ]
})
export class UsuarioModule { }
