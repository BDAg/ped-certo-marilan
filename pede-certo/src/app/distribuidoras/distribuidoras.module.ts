import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistribuidorasRoutingModule } from './distribuidoras-routing.module';
import { EscolherComponent } from './components/escolher/escolher.component';
import { ConfirmarComponent } from './components/confirmar/confirmar.component';
import { FinalizadoComponent } from './components/finalizado/finalizado.component';
import { GraphQLModule } from '../graphql.module';
import { FormsModule } from '@angular/forms';
import { ProdutosPedidoComponent } from './components/produtos-pedido/produtos-pedido.component';


@NgModule({
  declarations: [
    EscolherComponent, 
    ConfirmarComponent, 
    FinalizadoComponent, 
    ProdutosPedidoComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    DistribuidorasRoutingModule,
    GraphQLModule
  ],
  exports: [
    EscolherComponent, 
    ConfirmarComponent, 
    FinalizadoComponent
  ]
})
export class DistribuidorasModule { }
