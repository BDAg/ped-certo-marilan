import { Component, OnInit } from '@angular/core';
import { CriarPedidoGQL, MeGQL, InserirProdutoPedidoGQL } from 'src/app/services/pedeCertoGraphql.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html',
  styleUrls: ['./confirmar.component.css']
})
export class ConfirmarComponent implements OnInit {
  produtos: any[] = [];
  idUsuario;
  idPedido;
  constructor(
    private criarPedidoService: CriarPedidoGQL,
    private meService: MeGQL,
    private inserirProdutoPedidoService: InserirProdutoPedidoGQL,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.produtos = JSON.parse(localStorage.getItem('cart'));
    this.meService.fetch({}).subscribe(res => {
      this.idUsuario = res.data.me.id;
    });
  }

  removerProduto(id) {
    this.produtos = this.produtos.filter(produto => produto.id !== id);
    localStorage.setItem('cart', JSON.stringify(this.produtos));
  }

  get valorTotal(): number {
    return this.produtos.reduce((acc, produto) => acc + produto.total, 0);
  }

  confirmarPedido() {
    this.criarPedidoService.mutate({
      dealerId: this.idUsuario,
      preco_total: this.valorTotal,
      data_pedido: new Date().toLocaleDateString('en-US')
    }).toPromise().then(res => {
      this.idPedido = res.data.createPedido.id;
      this.produtos.forEach(produto => {
        this.inserirProdutoPedidoService.mutate({
          pedidoId: this.idPedido,
          produtoId: produto.id,
          preco: produto.preco,
          quantidade: produto.quantidade
        }).subscribe(data => {

        }, err => {
          console.error(err);
        });
      });
    }).then(res => {
      this.produtos = [];
      localStorage.setItem('cart', JSON.stringify(this.produtos));
      this.toast.sucesso('Pedido finalizado com sucesso');
    }).catch(err => {
      console.error(err);
      this.toast.erro('Erro ao finalizar pedido');
    });
  }

}
