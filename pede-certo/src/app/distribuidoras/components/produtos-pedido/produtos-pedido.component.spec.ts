import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutosPedidoComponent } from './produtos-pedido.component';

describe('ProdutosPedidoComponent', () => {
  let component: ProdutosPedidoComponent;
  let fixture: ComponentFixture<ProdutosPedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutosPedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutosPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
