import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProdutosPedidosGQL } from 'src/app/services/pedeCertoGraphql.service';

@Component({
  selector: 'app-produtos-pedido',
  templateUrl: './produtos-pedido.component.html',
  styleUrls: ['./produtos-pedido.component.css']
})
export class ProdutosPedidoComponent implements OnInit {
  idPedido;
  pageCount;
  hasNextPage;
  hasPrevPage;
  produtos: any[];
  constructor(
    private route: ActivatedRoute,
    private produtosPedidos: ProdutosPedidosGQL
  ) { }

  ngOnInit() {
    this.idPedido = this.route.snapshot.paramMap.get('id_pedido');
    this.produtosPedidos.fetch({
      page: 1,
      pageSize: 20,
      pedidoId: this.idPedido
    }).subscribe(res => {
      this.pageCount = res.data.findProdutoByPedido.pageInfo.pageCount;
      this.hasNextPage = res.data.findProdutoByPedido.pageInfo.hasNextPage;
      this.hasPrevPage = res.data.findProdutoByPedido.pageInfo.hasPrevPage;
      this.produtos = res.data.findProdutoByPedido.edges;
    }, err => {
      console.error(err);
    })
    console.log(this.idPedido);
  }

  metragemCubica(produto) {
    return produto.node.produto.altura * produto.node.produto.largura * produto.node.produto.profundidade;
  }

  peso(produto) {
    return produto.node.produto.peso * produto.node.quantidade;
  }

  get precoTotal() {
    return this.produtos.reduce((acc, produto) => acc + produto.node.pedido.preco_total, 0);
  }

  get quantidade() {
    return this.produtos.reduce((acc, produto) => acc + produto.node.quantidade, 0);
  }

  get pesoTotal() {
    return this.produtos.reduce((acc, produto) => acc + this.peso(produto), 0);
  }
  get metragemTotal() {
    return this.produtos.reduce((acc, produto) => acc + this.metragemCubica(produto), 0);
  }

}
