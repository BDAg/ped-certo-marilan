import { Component, OnInit } from '@angular/core';
import { GetProdutosGQL } from 'src/app/services/pedeCertoGraphql.service';
import { map } from 'rxjs/operators';
import { element } from 'protractor';
declare var $: any;
@Component({
  selector: 'app-escolher',
  templateUrl: './escolher.component.html',
  styleUrls: ['./escolher.component.css']
})
export class EscolherComponent implements OnInit {
  produtos: any;
  carrinho: any[];
  paginator = 1;
  hasNextPage: boolean;
  hasPrevPage: boolean;
  pageCount: number;
  pagination: number[] = [];

  constructor(
    private listarService: GetProdutosGQL
  ) { }

  ngOnInit() {
    this.getProdutos();
    this.carrinho = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
  }

  get produtosCarrinho(): number[] {
    return this.carrinho.map(produto => produto.id);
  }

  verificarProdutoCarrinho(id): boolean {
    return this.produtosCarrinho.includes(id);
  }

  adicionarProdutoCarrinho(produto: any, quantidade) {
    if (!quantidade) {
      return;
    }
    produto.quantidade = Number(quantidade);
    produto.total = Number((produto.preco * produto.quantidade).toFixed(2));
    this.carrinho.push(produto);
    localStorage.setItem('cart', JSON.stringify(this.carrinho));
  }

  removerProdutoCarrinho(id) {
    this.carrinho = this.carrinho.filter(produto => produto.id !== id);
    localStorage.setItem('cart', JSON.stringify(this.carrinho));
  }

  avancar() {
    this.paginator = this.paginator + 1;
    this.getProdutos();
  }

  voltar() {
    if (this.paginator > 1) {
      this.paginator = this.paginator - 1;
      this.getProdutos();
    }
  }

  irPagina(pagina) {
    this.paginator = pagina;
    this.getProdutos();
  }

  getProdutos() {
    this.listarService.fetch({ pageSize: 20, page: this.paginator }).subscribe(data => {
      this.produtos = data.data.produtos.edges;
      this.pageCount = data.data.produtos.pageInfo.pageCount;
      this.hasNextPage = data.data.produtos.pageInfo.hasNextPage;
      this.hasPrevPage = data.data.produtos.pageInfo.hasPrevPage;
      const pagination = new Array(data.data.produtos.pageInfo.pageCount).fill(0);
      pagination.forEach((pag, i) => {
        this.pagination[i] = i + 1;
      });
    });
  }
}
