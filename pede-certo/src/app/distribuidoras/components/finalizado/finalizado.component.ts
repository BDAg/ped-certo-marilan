import { Component, OnInit } from '@angular/core';
import { PedidosRealizadosGQL, MeGQL } from 'src/app/services/pedeCertoGraphql.service';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-finalizado',
  templateUrl: './finalizado.component.html',
  styleUrls: ['./finalizado.component.css']
})
export class FinalizadoComponent implements OnInit {
  idUsuario;
  pedidos: any[];

  constructor(
    private router: Router,
    private meService: MeGQL,
    private pedidosRealizados: PedidosRealizadosGQL,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.meService.fetch({}).subscribe(res => {
      this.idUsuario = res.data.me.id;
      this.pedidosRealizados.fetch({ userId: this.idUsuario }).subscribe(data => {
        this.pedidos = data.data.findPedidoByUser.edges;
      }, err => {
        console.error(err);
        this.toast.erro('Erro ao carregar pedidos');
      });
    }, err => {
      console.error(err);
      this.toast.erro('Erro ao carregar pedidos');
    });
  }

  visualizarProdutosPedido(id) {
    this.router.navigate([`/distribuidora/historico/${id}`]);
  }

}
