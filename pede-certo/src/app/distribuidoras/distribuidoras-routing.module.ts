import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DistribuidorasRoutingComponent } from './distribuidoras-routing.component';
import { EscolherComponent, ConfirmarComponent, FinalizadoComponent } from './components';
import { LayoutComponent } from '../layout/layout.component';
import { AuthGuard } from '../guards/auth.guard';
import { ProdutosPedidoComponent } from './components/produtos-pedido/produtos-pedido.component';


const routes: Routes = [
  {
    path: 'produtos',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ['ADMIN', 'SELLER', 'DEALER']
    },
    children: [
      {
        path: '',
        component: EscolherComponent
      },
    ]
  },
  {
    path: 'distribuidora',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ['ADMIN', 'DEALER']
    },
    children: [
      {
        path: 'confirmar',
        component: ConfirmarComponent
      },
      {
        path: 'historico',
        component: FinalizadoComponent
      },
      {
        path: 'historico/:id_pedido',
        component: ProdutosPedidoComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/produtos'
      }
    ]
  }
];

@NgModule({
  declarations: [
    DistribuidorasRoutingComponent
  ],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class DistribuidorasRoutingModule { }
