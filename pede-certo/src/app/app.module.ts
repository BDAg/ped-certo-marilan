import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { EmpresasModule } from './empresas/empresas.module';
import { DistribuidorasModule } from './distribuidoras';
import { UsuarioModule } from './usuario/usuario.module';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { Interceptor } from './auth/intercept.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    LayoutModule,
    BrowserModule,
    EmpresasModule,
    DistribuidorasModule,
    UsuarioModule,
    GraphQLModule,
    HttpClientModule,
    Interceptor,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
    ,
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
