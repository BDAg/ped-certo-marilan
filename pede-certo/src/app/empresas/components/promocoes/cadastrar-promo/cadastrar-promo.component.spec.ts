import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarPromoComponent } from './cadastrar-promo.component';

describe('CadastrarPromoComponent', () => {
  let component: CadastrarPromoComponent;
  let fixture: ComponentFixture<CadastrarPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
