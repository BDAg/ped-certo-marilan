import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarPromoComponent } from './listar-promo.component';

describe('ListarPromoComponent', () => {
  let component: ListarPromoComponent;
  let fixture: ComponentFixture<ListarPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
