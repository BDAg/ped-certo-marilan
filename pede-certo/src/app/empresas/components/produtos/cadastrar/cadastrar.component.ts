import { Component, OnInit } from '@angular/core';
import { CadastrarProdutoGQL, CategoriasProdutosGQL, DadosProdutoGQL, EditarProdutoGQL } from 'src/app/services/pedeCertoGraphql.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/services/toast.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {
  form: FormGroup;
  idProduto: string;
  categorias: any[];
  previewImg: any;
  submitted = false;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  // imagem_inserida = false;

  constructor(
    private cadastrarProdutoService: CadastrarProdutoGQL,
    private editarProdutoService: EditarProdutoGQL,
    private categoriasProdutosService: CategoriasProdutosGQL,
    private dadosProdutoService: DadosProdutoGQL,
    private formBuilder: FormBuilder,
    private toast: ToastService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.idProduto = this.route.snapshot.paramMap.get('id_produto');
    this.criarForm();
    if (this.idProduto) {
      this.form.get('categoriaId').disable();
      this.dadosProdutoService.fetch({ id: this.idProduto })
        .subscribe(res => {
          this.form.get('nome').setValue(res.data.produto.nome);
          this.form.get('preco').setValue(res.data.produto.preco.toFixed(2));
          this.form.get('descricao').setValue(res.data.produto.descricao);
          this.form.get('image_src').setValue(res.data.produto.image_src);
          this.form.get('categoriaId').setValue(res.data.produto.categoria.id);
        }, error => {
          this.toast.erro('Erro ao carregar os dados do produto');
        });
    }
    this.getCategorias();
  }

  criarForm() {
    this.form = this.formBuilder.group({
      categoriaId: [null, Validators.required],
      nome: [null, Validators.required],
      descricao: [null],
      preco: [null, Validators.required],
      altura: [null, Validators.required],
      largura: [null, Validators.required],
      profundidade: [null, Validators.required],
      peso: [null, Validators.required],
      image_src: [null, Validators.required]
    });
  }

  getCategorias() {
    this.categoriasProdutosService.fetch({ pageSize: 100 }).pipe(
      map(res => res.data.categorias.edges)
    ).subscribe(data => {
      this.categorias = data;
    }, error => {
      this.toast.erro('Erro ao carregar as categorias de produtos');
    });
  }

  salvar() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    if (this.idProduto) {
      this.form.get('categoriaId').enable();
      this.submitted = true;
      this.editarProdutoService.mutate(Object.assign({ id: this.idProduto }, this.form.value)).subscribe(res => {
        this.form.get('categoriaId').disable();
        this.submitted = false;
        this.toast.sucesso('Produto atualizado com sucesso');
      }, error => {
        this.form.get('categoriaId').disable();
        this.toast.erro('Erro ao cadastrar produto');
        this.submitted = false;
      });
    } else {
      this.cadastrarProdutoService.mutate(this.form.value).subscribe(res => {
        this.previewImg = null;
        this.toast.sucesso('Produto cadastrado com sucesso');
        this.form.reset();
        this.submitted = false;
      }, error => {
        this.toast.erro('Erro ao cadastrar produto');
        this.submitted = false;
      });
    }
  }

  get image_src(): string {
    return this.form.get('image_src').value;
  }

  onFileChange(event) {
    if (event && event[0] && event[0].type.startsWith('image')) {
      const reader = new FileReader();
      reader.readAsDataURL(event[0]);
      reader.onload = e => {
        this.form.get('image_src').setValue(reader.result);
      };
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
}
