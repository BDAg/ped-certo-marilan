import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetProdutosGQL, DeletarProdutoGQL, Categoria, CategoriasProdutosGQL } from 'src/app/services/pedeCertoGraphql.service';
import { map } from 'rxjs/operators';
declare const $: any;
@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  produtos: any[] = [];
  categorias: any;
  categoria = 0;
  constructor(
    private router: Router,
    private listarService: GetProdutosGQL,
    private empresaService: DeletarProdutoGQL,
    private categoriaService: CategoriasProdutosGQL
  ) { }


  ngOnInit() {
    this.listarProdutos();
    this.getCategorias();
  }

  listarProdutos() {
    this.listarService.fetch({ pageSize: 10, page: 1 }).pipe(
      map(data => data.data.produtos.edges)
    ).subscribe(data => {
      this.produtos = data;
    });
  }

  irAdicionarProduto() {
    this.router.navigate(['/empresa/produtos/cadastrar']);
  }

  irEditarProduto(id: string) {
    this.router.navigate([`/empresa/produtos/editar/${id}`]);
  }

  deletarProduto(id: string) {
    this.empresaService.mutate({ ID: id }).subscribe(data => {
      this.listarProdutos();
    }, error => {
      console.log(error);
    }
    );
  }

  getCategorias() {
    this.categoriaService.fetch({ pageSize: 10, page: 1 }).pipe(
      map(data => data.data.categorias.edges)
    ).subscribe(data => {
      this.categorias = data;
    });
  }

  get filtrarCategorias() {
    if (this.categoria == 0) {
      return this.produtos;
    } else {
      return this.produtos.filter(produto => this.categoria === produto.node.categoria.id);
    }
  }

}
