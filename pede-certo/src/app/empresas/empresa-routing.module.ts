import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpresaRoutingComponent } from './empresa-routing.component';

import { ListarComponent, CadastrarComponent, ListarPromoComponent, CadastrarPromoComponent, EditarPromoComponent } from './components';
import { PedidosComponent } from './components';
import { LayoutComponent } from '../layout/layout.component';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: 'empresa',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ['ADMIN', 'SELLER']
    },
    children: [
      {
        path: 'pedidos',
        component: PedidosComponent,
      },
      {
        path: 'produtos',
        component: ListarComponent,
      },
      {
        path: 'produtos/cadastrar',
        component: CadastrarComponent
      },
      {
        path: 'produtos/editar/:id_produto',
        component: CadastrarComponent
      },
      {
        path: 'promocoes',
        component: ListarPromoComponent,
      },
      {
        path: 'promocoes/editar',
        component: EditarPromoComponent
      },
      {
        path: 'promocoes/cadastrar',
        component: CadastrarPromoComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'produtos'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    EmpresaRoutingComponent
  ],
  providers: [
    AuthGuard
  ]
})
export class EmpresaRoutingModule { }
