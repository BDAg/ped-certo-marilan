import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
//Rotas
import { EmpresaRoutingModule } from './empresa-routing.module';
// Produtos
import { CadastrarComponent } from './components';
import { ListarComponent } from './components';
//pedidos
import { PedidosComponent } from './components';
//promocoes
import { EditarPromoComponent } from './components';
import { CadastrarPromoComponent } from './components';
import { ListarPromoComponent } from './components';
import { GraphQLModule } from '../graphql.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
@NgModule({
  declarations: [
    CadastrarComponent,
    ListarComponent,
    PedidosComponent,
    EditarPromoComponent,
    CadastrarPromoComponent,
    ListarPromoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmpresaRoutingModule,
    GraphQLModule,
    ImageCropperModule
  ],
  exports: [
    CadastrarComponent,
    ListarComponent,
    PedidosComponent,
    EditarPromoComponent,
    CadastrarPromoComponent,
    ListarPromoComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EmpresasModule { }
